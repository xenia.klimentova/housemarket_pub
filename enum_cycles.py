""" Module to enumerate all cycles from the graph 

    note: this does not work directly with data read from legacy_kep_io.py
    because the adj structure is not compatible, check the new kep_io.py 
    (repair_old_data) for necessary steps for the repair
"""

class cctuple(tuple):
    """ derived tuple that has a boolean indicator ._chain (default False) """
    
    def __new__(cls, lst, chain=False):
        return super().__new__(cls, lst)
    
    def __init__(self, lst, chain=False):
        self._chain = chain
        
        
def get_arcs(c):
    """ get the arcs of a chain or a cycle defined by cctuple structure """
    
    return [(c[i], c[i+1]) for i in range(len(c)-1)] if c._chain \
           else [(c[i-1], c[i]) for i in range(len(c))]
    
    
def all_cycles(cycles, path, node, tovisit, adj, K, checker=None):
    """ recursive function to open new nodes and detect cycles that loop 
        back to path[0], checker can be used to further valid/reject cycles
    """ 

    for i in adj[node]: # analogous to depth-first search but more extensive
        if i in tovisit: 
            if K-1>0 and i>path[0]: # this guarantees that paths are normalized
                all_cycles(cycles, path + [node], i, tovisit - set([i]), adj, K - 1, checker)
        elif i==path[0]:
            newc = cctuple(path + [node])
            if checker is None or checker(newc):
                cycles.append(newc)
        
def get_all_cycles(nodes, adj, K, checker=None):
    """ enumerate all cycles of length no more than K and connected with 
        a specific set of nodes, checker can be used to further valid/reject cycles
    """
    
    cycles = []
    if K>=2:
        for i in nodes: 
            for j in adj[i]:
                if j>i: # this guarantees that paths are normalized
                    all_cycles(cycles, [i], j, nodes - set([i, j]), adj, K - 1, checker)
    return cycles


def all_chains(chains, path, node, tovisit, adj, K, checker=None):
    """ recursive function to open new nodes and add the explored path to chains 
    """
    
    newc = cctuple(path + [node], chain=True)
    if checker is None or checker(newc):
        chains.append(newc)

    for i in adj[node]: # analogous to depth-first search but more extensive
        if K-1>0 and i in tovisit: 
            all_chains(chains, path + [node], i, tovisit - set([i]), adj, K - 1, checker)


def get_all_chains(altnodes, adj, K, checker=None):
    """ enumerate all chains of length no more than K and connected with 
        a specific set of nodes (altnodes)
    """
    
    nodes, chains = set(range(len(adj))), []
    if K>=2:
        for i in altnodes: 
            for j in adj[i]:
                all_chains(chains, [i], j, nodes - set([i, j]), adj, K - 1, checker)
    return chains
    

def get_all_cycles_and_chains(altnodes, adj, K, L, checker=None):
    """ interface function to get all cycles and chains """
    
### test for duplication
#    cycles = get_all_cycles(set(adj) - altnodes, adj, K)
#    print("\n", len(cycles), len(set(cycles)))
#    chains = get_all_chains(           altnodes, adj, H)
#    print("\n", len(chains), len(set(chains)))
#    return cycles + chains
    
    return   get_all_cycles(set(adj) - altnodes, adj, K, checker) \
           + get_all_chains(           altnodes, adj, L, checker)
    
