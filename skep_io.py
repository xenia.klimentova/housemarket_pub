#from skeputils import *
def read_data(filename):
	f = open(filename)
	data = f.read().split()
	nverts = int(data.pop(0))
	narcs = int(data.pop(0))
	npairs = int(data.pop(0))
	nalt = int(data.pop(0))
	narcs_origin = int(data.pop(0))
	arcs = set()
	w = {}
	r = {}
	V = set()
	print("V ", nverts, "A ", narcs)
	for a in range(narcs):
		i = int(data.pop(0))
		j = int(data.pop(0))
		assert i >= 0 and j >= 0
		V.add(i)
		V.add(j)
		arcs.add((i, j))
		w[i, j] = float(data.pop(0))
		r[i, j] = int(data.pop(0))
	if (len(V) != nverts):
		print("len V != nverts", len(V), nverts)
		print(V)
		exit(1)
	return V, arcs, w, r

