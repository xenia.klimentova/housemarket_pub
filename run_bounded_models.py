from skep_io import *
from hm_utils import *
from enum_cycles import *
from time import process_time
from modelling import *
import random
import copy
import run_models
numthreads = 1
mymipgap = 0.0
maxtime = 900
presolve = 0


def model(mod, resfile, newline, endline):

	model,y, lpfile = run_models.make_model(mod,m)
	#model.write(lpfile)
	optimise_model(model, y, mod, resfile, newline, endline)
	#input()

def optimise_model(model, y_vars, mod, fileoutres, nl,eol):

	model.setParam("TimeLimit", maxtime)
	if(presolve == 0):
		model.setParam("presolve", 0)
	print("presolve", model.getParamInfo('presolve'))


	mipobj = None
	# =============== mip solve =======================

	strt = process_time()


	# m_pres = model.presolve()
	# m_pres.write(str(mod)+"pres.lp")
	# input()
	model.optimize()
	# status codes:
	#	2 - optimal
	#	9 - time limit
	#	3 - infeasible
	status = model.status
	if (status != 2 and status != 9 and status != 3):
		print("Model status %d. Exiting" % model.status)
		exit(1)
	mipt = process_time() - strt

	print("Mip solved with status", status, "in ", mipt, " seconds")

	price_fairness_max = None
	price_fairness_weight = None

	sb = None
	sb_2= None
	sb_3 = None

	wb = None
	wb_2 = None
	wb_3 = None
	num_trans = None
	tot_weight = None

	s_nv_better =None
	s_av_c_better = None
	w_nv_better = None
	w_av_c_better = None
	unsolved = 0
	if (status == 2 or status == 9):
		mipobj = model.objVal
		if mipobj<0:
			unsolved = 1
		sol = {a for a in arcs if y_vars[a].x > 0.5}
		num_trans = run_models.calc_trans(sol)
		tot_weight = run_models.calc_weight(sol,m.w)
		#if(mipobj > 0):

		if (status == 9):
			bestbound = model.ObjBoundC
		if(mod == 112):
			global MaxNumTrans
			MaxNumTrans = mipobj
			global AggTrans
			AggTrans+=mipobj
			global AggTrans_weight
			AggTrans_weight+=tot_weight
		elif(mod == 223):
			global MaxWeightTrans
			MaxWeightTrans = mipobj
			global AggWeight
			AggWeight += mipobj
			global AggWeight_trans
			AggWeight_trans += num_trans

		# ------------ price of fairness + global variables saved -------------------------

		elif(mod>13 and mod<20):
			price_fairness_max = 1.0*(MaxNumTrans - num_trans)/MaxNumTrans*100

			if (mod == 14):
				global PFt_core
				PFt_core += price_fairness_max
				global AggTr_core
				AggTr_core += num_trans
			if (mod == 15):
				global PFt_wakocore
				PFt_wakocore += price_fairness_max
				global AggTr_wakocore
				AggTr_wakocore += num_trans
			if (mod == 16):
				global PFt_stcore
				PFt_stcore += price_fairness_max
				global AggTr_stcore
				AggTr_stcore += num_trans

		elif (mod > 23 and mod < 30):
			price_fairness_weight = 1.0*(MaxWeightTrans - tot_weight) / MaxWeightTrans * 100

			if (mod == 24):
				global PFw_core
				PFw_core += price_fairness_weight
				global AggW_core
				AggW_core += tot_weight
			if (mod == 25):
				global PFw_wakocore
				PFw_wakocore += price_fairness_weight
				global AggW_wakocore
				AggW_wakocore += tot_weight

			if (mod == 26):
				global PFw_stcore
				PFw_stcore += price_fairness_weight
				global AggW_stcore
				AggW_stcore += tot_weight

		print("Check blocking cycles K=", K)
		sb, sb_2, sb_3,bbb,bbb,bbb, s_nv_better, s_av_c_better = run_models.check_blocking_cycles(m, sol, 'S', K)
		wb, wb_2, wb_3,bbb,bbb,bbb, w_nv_better, w_av_c_better = run_models.check_blocking_cycles(m, sol, 'W', K)

		print("Blocking cycles: sb", sb, " wb ", wb)
		## ----- blocking for max n trans --------------------
		if (mod == 112):
			global SBtr3
			SBtr3 += sb_2 + sb_3
			global WBtr3
			WBtr3 += wb_2 + wb_3

			global SBtr2
			SBtr2 += sb_2
			global WBtr2
			WBtr2 += wb_2

		if (mod == 14):
			global SBtr_core3
			SBtr_core3 += sb_2 + sb_3
			global SBtr_core2
			SBtr_core2 += sb_2

			global WBt_core3
			WBt_core3 += wb_2+wb_3
			global WBt_core2
			WBt_core2 += wb_2

		if (mod == 15):
			global SBtr_wakocore3
			SBtr_wakocore3 += sb_2 + sb_3
			global SBtr_wakocore2
			SBtr_wakocore2 += sb_2

			global WBt_wakocore3
			WBt_wakocore3 += wb_2+wb_3
			global WBt_wakocore2
			WBt_wakocore2 += wb_2

		if (mod == 16):
			global SBtr_stcore3
			SBtr_stcore3 += sb_2 + sb_3
			global SBtr_stcore2
			SBtr_stcore2 += sb_2

			global WBt_stcore3
			WBt_stcore3 += wb_2+wb_3
			global WBt_stcore2
			WBt_stcore2 += wb_2

		## ----- blocking for max weight trans --------------------
		if (mod == 223):
			global SBweight3
			SBweight3 += sb_2 + sb_3
			global WBweight3
			WBweight3 += wb_2 + wb_3

			global SBweight2
			SBweight2 += sb_2
			global WBweight2
			WBweight2 += wb_2

		if (mod == 24):
			global SBweight_core3
			SBweight_core3 += sb_2 + sb_3
			global SBweight_core2
			SBweight_core2 += sb_2

			global WBw_core3
			WBw_core3 += wb_2 + wb_3
			global WBw_core2
			WBw_core2 += wb_2

		if (mod == 25):
			global SBweight_wakocore3
			SBweight_wakocore3 += sb_2 + sb_3
			global SBweight_wakocore2
			SBweight_wakocore2 += sb_2

			global WBw_wakocore3
			WBw_wakocore3 += wb_2 + wb_3
			global WBw_wakocore2
			WBw_wakocore2 += wb_2

		if (mod == 26):
			global SBweight_stcore3
			SBweight_stcore3 += sb_2 + sb_3
			global SBweight_stcore2
			SBweight_stcore2 += sb_2

			global WBw_stcore3
			WBw_stcore3 += wb_2 + wb_3
			global WBw_stcore2
			WBw_stcore2 += wb_2

	else:
		if (mod != 13 and mod !=23):
			print("Something went wrong, usolved model is not strong core (13, 23)", mod)
			exit()
		if (mod == 13):
			global Nsolved_Tstcore
			Nsolved_Tstcore -= 1
		if (mod == 23):
			global Nsolved_Wstcore
			Nsolved_Wstcore -= 1

	print("Mip solved =", mipobj, " with status", status, "in ", mipt, " seconds")

	global AggTime
	AggTime+=mipt

	# -------------- write results ------------------------
	ftex = open(fileoutres, 'a')
	if(nl == True):
		ftex.write(instname + "\t" + str(len(V)) + "\t" + str(len(arcs)) + "\t" + str(presolve))# +"\t" + "%.2f" % preptime + "\t"

	ftex.write("\t" + str(mod) + "\t" + str(status) + "\t" + str(unsolved)+"\t" + str(mipobj) +"\t"+ str(mipt)+
				"\t" + str(num_trans) + "\t" + str(tot_weight)+
			   "\t"+str(sb)+"\t" + str(sb_2) + "\t" + str(sb_3) +
				"\t" + str(s_nv_better) + "\t" + str(s_av_c_better)+
			   "\t"+str(wb)+"\t" + str(wb_2) + "\t" + str(wb_3) +
				"\t" + str(w_nv_better) + "\t" + str(w_av_c_better))

	if(mod>10 and mod<20):
		ftex.write("\t"+str(price_fairness_max))
	if(mod>20 and mod<30):
		ftex.write("\t" + str(price_fairness_weight))

	if (el):
		ftex.write("\n")
	ftex.close()
# -----------------------------------------------------

	# if (mod == 888 or mod == 999 or mod == 222):
	# 	global ObjMaxCF
	# 	ObjMaxCF = mipobj
	# 	if(kappa < 0 and mod == 999):
	# 		error("No kappa defined. No relaxing stability calculations" )
	# 	if(mod == 999):
	# 		global rhsdelta
	# 		rhsdelta = ObjMaxCF - round(kappa * ObjMaxCF)
	# 		writerespirce(mod, opt, None, None, inittime, mipt, newline, endline)
	#
	# 	if(mod == 888 or mod ==222):
	# 		writeres(mod, None, mipobj, None, None, opt, inittime,
	# 			 None, mipt, ncols, nrows, nnze, newline, endline)


if __name__ == '__main__':
	import sys

	try:
		preferences = sys.argv[5]

	except:
		preferences = "S"

	print("======================================================")
	print("\t Run settings:")
	print("\t\tpreferences \t", preferences)
	print("======================================================")

	#Sizes = [3]
	#Sizes = [20,30,40,50,60,70,80,90,100,110,120,130,140,150]
	Sizes = [10]
	cycles_sizes = [2,3]
	#numinst = 3
	instancesset = range(3)


	#instancesset = [10]

	for K in cycles_sizes:
		for size in Sizes:

			AggTime = 0.0
			Nsolved_Tstcore = len(instancesset)
			Nsolved_Wstcore = len(instancesset)

			AggTrans = 0
			AggTr_core = 0
			AggTr_wakocore = 0
			AggTr_stcore = 0

			AggWeight_trans = 0
			AggW_core_trans = 0
			AggW_wakocore_trans = 0
			AggW_stcore_trans = 0


			AggWeight = 0
			AggW_core = 0
			AggW_wakocore = 0
			AggW_stcore = 0

			AggTrans_weight = 0
			AggTr_core_weight = 0
			AggTr_wakocore_weight = 0
			AggTr_stcore_weight = 0


			PFt_core = 0.0
			PFt_wakocore = 0.0
			PFt_stcore = 0.0

			PFw_core = 0.0
			PFw_wakocore = 0.0
			PFw_stcore = 0.0


			SBtr3 = 0
			SBweight3 = 0
			SBtr2 = 0
			SBweight2 = 0

			SBtr_core3 = 0
			SBweight_core3 = 0
			SBtr_core2 = 0
			SBweight_core2 = 0

			SBtr_wakocore3 = 0
			SBweight_wakocore3 = 0
			SBtr_wakocore2 = 0
			SBweight_wakocore2 = 0

			SBtr_stcore3 = 0
			SBweight_stcore3 = 0
			SBtr_stcore2 = 0
			SBweight_stcore2 = 0


			WBtr3 = 0
			WBt_core3 = 0
			WBt_wakocore3 = 0
			WBt_stcore3 = 0

			WBtr2 = 0
			WBt_core2 = 0
			WBt_wakocore2 = 0
			WBt_stcore2 = 0

			WBweight3 = 0
			WBw_core3 = 0
			WBw_wakocore3 = 0
			WBw_stcore3 = 0

			WBweight2 = 0
			WBw_core2 = 0
			WBw_wakocore2 = 0
			WBw_stcore2 = 0

			for inst in instancesset:
				if (preferences == "S"):
					filename = "KEP_strict/%d_%d.input" % (size, inst + 1,)
					outfolder = "Strict_results/"
				elif (preferences == "N"):
					filename = "KEP_ties/n%d_%d.input" % (size, inst + 1,)
					outfolder = "Weak_results/"
				else:
					print("Error: Preferences parameter not defined: S or N")
					exit(1)

				sstr1 = filename.replace("KEP_ties/", "")
				sstr = sstr1.replace("KEP_strict/", "")
				instname = sstr.replace(".input", "")
				starttime = process_time()

				V, arcs, w, r = read_data(filename)

				# ======================================
				maxr = max(r[a] for a in arcs)
				if (maxr > len(V)):
					print("something wrong with max rank, should be <=V")
					exit(1)

				# ======================================

				#------- add self loops, if they does not exist
				random.seed(1)

				for i in V:
					if((i,i) not in arcs):
						arcs.add((i, i))
						r[i, i] = len(V) + 1
						w[i, i] = 1.0

				# print(arcs)
				# input()

				adj_in, adj_out = makeadj(arcs, V, r)
				# ----------------------------------------------
				# arcs are already with dummy acrs to NDDs.
				# This code only works for K = L
				# ----------------------------------------------

				C = get_all_cycles(V, adj_out, K)
				numCycles = len(C)

				print("Cycles and chains found")
				for v in V:
					C.append((v, v))
				# print(C)
				# for ic in range(len(C)):
				# 	print(ic, C[ic])
				vinc = {}
				for v in V:
					vinc[v] = [ic for ic in range(len(C)) if v in C[ic]]

				print("vinc made")
				# print(vinc)
				ainc = {}
				for a in arcs:
					ainc[a] = []

				for c in C:
					ic = C.index(c)
					for i in range(len(c)):
						a = (c[i - 1], c[i])
						ainc[a].append(ic)
						if (c[i - 1] == c[i]):
							break
				# print(ainc)


				# print("Cycles and chains found:", numCycles)

				#---------------------


				m = modelling()
				m.V = V
				m.arcs = arcs
				m.r = r
				m.adj_out = adj_out
				m.adj_in = adj_in
				m.w = w

				m.C = C
				m.vinc = vinc
				m.ainc = ainc

				file_full_out = "full.out"
				modcodes = [112,14,15,16]
				for mm in modcodes:
					nl = False
					el = False
					if (modcodes.index(mm) == 0):
						nl = True
					if (modcodes.index(mm) == len(modcodes) - 1):
						el = True

					#model(mm, outfolder +"max_trans.out", nl, el)
					file_full_out = outfolder + "max_trans_"+str(K)+".out"
					model(mm,  file_full_out, nl, el)
					print("model %d is done \n================================\n\n" % (mm,))
				#	input()
				if inst == len(instancesset) - 1:
					fff = open(file_full_out, "a")
					fff.write("\n\n\n")
					fff.close()

				modcodes = [223, 24,25,26]
				for mm in modcodes:
					nl = False
					el = False
					if (modcodes.index(mm) == 0):
						nl = True
					if (modcodes.index(mm) == len(modcodes) - 1):
						el = True
					#model(mm, outfolder +"max_weight.out", nl, el)
					file_full_out = outfolder + "max_weight_"+str(K)+".out"
					model(mm, file_full_out, nl, el)
					print("model %d is done \n================================\n\n" % (mm,))

				if inst == len(instancesset) - 1:
					fff = open(file_full_out, "a")
					fff.write("\n\n\n")
					fff.close()

			# ===============================================================================
			# ======== print average values ================================================
			# ===============================================================================

			#----- max number of transplants --------------------------
			fpic = open(outfolder + "abs_trans_"+str(K)+".out", 'a')
			fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%d\n") % (size,1.0 * AggTrans / len(instancesset),
													 1.0 * AggTr_core / len(instancesset),
													 1.0 * AggTr_wakocore / len(instancesset),
													 1.0 * AggTr_stcore / Nsolved_Tstcore,
															   Nsolved_Tstcore))
			fpic.close()

			# ----- max weight of transplants --------------------------

			fpic = open(outfolder +"abs_weight_"+str(K)+".out", 'a')
			fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%d\n") % (size, 1.0 * AggWeight / len(instancesset),
													 1.0 * AggW_core / len(instancesset),
													 1.0 * AggW_wakocore / len(instancesset),
													 1.0 * AggW_stcore / Nsolved_Wstcore,
															   Nsolved_Wstcore))
			fpic.close()

			#----- price of fairness max trans --------------------------
			fpic = open(outfolder + "pf_trans_"+str(K)+".out", 'a')
			fpic.write(("%d\t%.2f\t%.2f\t%.2f\n")%
					   (size, 1.0*PFt_core/len(instancesset),
						1.0*PFt_wakocore/len(instancesset),
						1.0*PFt_stcore/ Nsolved_Tstcore ,
						)
					   )
			fpic.close()

			# ----- price of fairness max weight --------------------------
			fpic = open(outfolder + "pf_weight_"+str(K)+".out", 'a')
			fpic.write(("%d\t%.2f\t%.2f\t%.2f\n")%(size, 1.0*PFw_core/len(instancesset),
												   1.0*PFw_wakocore/len(instancesset),
												   1.0*PFw_stcore/ Nsolved_Wstcore, ))
			fpic.close()

			# ------- blocking cycles max trans -------------------------------------
			fpic = open(outfolder + "strongly_blocking_trans_"+str(K)+".out", 'a')
			fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n")
					   % (size, 1.0 * SBtr2 / len(instancesset),
											   1.0 * SBtr3 / len(instancesset),
											   1.0 * SBtr_core2 / len(instancesset),
											   1.0 * SBtr_core3 / len(instancesset),
											   1.0 * SBtr_wakocore2 / len(instancesset),
											   1.0 * SBtr_wakocore3 / len(instancesset),
											   1.0 * SBtr_stcore2 / len(instancesset),
											   1.0 * SBtr_stcore3 / len(instancesset)
											   	 ))
			fpic.close()
			# ------- blocking cycles max weight-------------------------------------
			fpic = open(outfolder + "strongly_blocking_weight_"+str(K)+".out", 'a')
			fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n") %
					   (size, 1.0 * SBweight2 / len(instancesset),
														   1.0 * SBweight3 / len(instancesset),
											   1.0 * SBweight_core2 / len(instancesset),
											   1.0 * SBweight_core3 / len(instancesset),
											   1.0 * SBweight_wakocore2 / len(instancesset),
											   1.0 * SBweight_wakocore3 / len(instancesset),
											   1.0 * SBweight_stcore2 / len(instancesset),
											   1.0 * SBweight_stcore3 / len(instancesset),
														   ))
			fpic.close()

			# -------weakly blocking cycles max trans-------------------------------------
			fpic = open(outfolder + "weakly_blocking_trans_"+str(K)+".out", 'a')
			fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n") % (size,
													 1.0 * WBtr2 / len(instancesset),
													 1.0 * WBtr3 / len(instancesset),
													 1.0 * WBt_core2 / len(instancesset),
													 1.0 * WBt_core3 / len(instancesset),
													 1.0 * WBt_wakocore2 / len(instancesset),
													 1.0 * WBt_wakocore3 / len(instancesset),
												   1.0 * WBt_stcore2 / len(instancesset),
												   1.0 * WBt_stcore3 / len(instancesset),

																	   ))
			fpic.close()


			# -------weakly blocking cycles max weight-------------------------------------
			fpic = open(outfolder + "weakly_blocking_weight_"+str(K)+".out", 'a')
			fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n")% (size,
						  1.0 * WBweight2 / len(instancesset),
						  1.0 * WBweight3 / len(instancesset),
						  1.0 * WBw_core2 / len(instancesset),
						  1.0 * WBw_core3 / len(instancesset),
						  1.0 * WBw_wakocore2 / len(instancesset),
						  1.0 * WBw_wakocore3 / len(instancesset),
						  1.0 * WBw_stcore2 / len(instancesset),
						  1.0 * WBw_stcore3 / len(instancesset),

																	  ))
			fpic.close()

