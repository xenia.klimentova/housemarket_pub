from skep_io import *
from hm_utils import *
from enum_cycles import *
from time import process_time
from modelling import *
import random
import copy
numthreads = 1
mymipgap = 0.0
maxtime = 900
presolve = 0

MaxNumTrans = 0
MaxWeightTrans = 0


AggTime = 0.0
Unsolved = 0

AggTrans = 0
AggTr_core =0
AggTr_compet = 0
AggTr_stcore = 0

AggWeight = 0
AggW_core =0
AggW_compet = 0
AggW_stcore = 0


PFt_core = 0.0
PFt_compet = 0.0
PFt_stcore = 0.0

PFw_core = 0.0
PFw_compet = 0.0
PFw_stcore = 0.0

SBtr5 = 0
SBweight5 = 0
SBtr4 = 0
SBweight4 = 0
SBtr3 = 0
SBweight3 = 0
SBtr2 = 0
SBweight2 = 0


WBtr5 = 0
WBt_core5 = 0
WBt_compet5 = 0
WBtr4 = 0
WBt_core4 = 0
WBt_compet4 = 0
WBtr3 = 0
WBt_core3 = 0
WBt_compet3 = 0
WBtr2 = 0
WBt_core2 = 0
WBt_compet2 = 0


WBweight5 = 0
WBw_core5 = 0
WBw_compet5 = 0
WBweight4 = 0
WBw_core4 = 0
WBw_compet4 = 0
WBweight3 = 0
WBw_core3 = 0
WBw_compet3 = 0
WBweight2 = 0
WBw_core2 = 0
WBw_compet2 = 0


def prepare(model):
	model.setParam("LogToConsole", 0)
	model.setParam("Threads", numthreads)
	#model.setParam("MarkowitzTol ", 1e-4)
	#model.setParam("Method", 1)
	# model.setParam("MIPGap", mymipgap)
	# model.setParam("FeasibilityTol", 1e-9)
	# model.setParam("IntFeasTol", 1e-9)
	# model.setParam("MIPGapAbs", mymipgap)
	# model.setParam("OptimalityTol", 1e-9)
	#model.setParam("Presolve", 0)

#walking through the best arcs, find a cycle
def find_c(adj_out,ii,path):
	j = adj_out[ii][0]
	for k in range(len(path)):
		if j == path[k]:
			return path[k:]
	path.append(j)
	return find_c(adj_out,j,path)

def TTC(adj_out, r):
	cycles = []
	random.seed(1)
	while(True):
		if(len(adj_out) == 0):
			break
		ind_i = list(adj_out.keys()).pop(int(len(adj_out.keys())*random.random()))

		c = find_c(adj_out, ind_i, [ind_i])

		cycles.append(c)
		for i in c:
			del adj_out[i]

		for i in c:
			for ii in adj_out.keys():
				if i in adj_out[ii]:
					adj_out[ii].remove(i)
	return cycles

def check_improve_vert(m,bcycles,sol):

	bool_improve = [0 for v in m.V]
	num_improve = [0 for v in m.V]
	arc_sol = None
	for c in bcycles:
		for i in range(len(c)):
			for a in sol:
				if a[0] == c[i-1]:
					arc_sol = a
					break
			arc_c = (c[i-1],c[i])
			if(m.r[arc_c] < m.r[arc_sol]):
				bool_improve[a[0]] = 1
				num_improve[a[0]] +=1

	# print(sol)
	# print(bcycles)
	# print(bool_improve)
	# print(num_improve)

	return sum(bool_improve), 1.0*sum(num_improve)/len(m.V)

def check_blocking_cycles(m,sol,type_bc,K):
	# -----------------------------------------------------------
	# type_bc is the type of blocking cycle we are checking:
	# 	S - is the strongly blocking cycles
	#	W - is the weakly blocking cycles
	# -----------------------------------------------------------


	# print("solution")
	# for a in sol:
	# 	print(a, r[a])

	adj_out_dominate ={v:set() for v in m.V}
	arcs_equal = set()
	adj_out_weak_dominate = {v:set() for v in m.V}

	for a in sol:
		for v in m.adj_out[a[0]]:
			arc = (a[0], v)
			if (m.r[a] > m.r[arc]):
				adj_out_dominate[arc[0]].add(arc[1])
			if (m.r[a] == m.r[arc]):
				arcs_equal.add(arc)
			if(m.r[a]>= m.r[arc]):
				adj_out_weak_dominate[arc[0]].add(arc[1])

	#print("dominating arcs:")
	for v in adj_out_dominate.keys():
		for vv in adj_out_dominate[v]:
			arc = (v,vv)
			#print(arc, r[arc])
	#print("weakly dominating arcs:")
	for v in adj_out_weak_dominate.keys():
		for vv in adj_out_weak_dominate[v]:
			arc = (v, vv)
			#print(arc, r[arc])

	# print("equal arcs:")
	# for a in arcs_equal:
	# 	print(a, r[a])
	# input()
	if(type_bc == 'S'):
		bcycles = get_all_cycles(m.V,adj_out_dominate, K)
	elif(type_bc == 'W'):
		bcycles = get_all_cycles(m.V, adj_out_weak_dominate, K)
		nonblocking = set()
		for c in bcycles:
			count_equal = 0
			for i in range(len(c)):
				if((c[i-1],c[i]) in arcs_equal):
					count_equal+=1
			if(count_equal == len(c)):
				nonblocking.add(c)
		for nbc in nonblocking:
			bcycles.remove(nbc)
	else:
		print("Type of blocking cycles type_bc is incorrect", type_bc)
		exit(1)

	print("\tNumber of blocking cycles", len(bcycles))
	#print(bcycles)

	nvert_improve, aver_nbc_improve_vert = check_improve_vert(m,bcycles,sol)

	b =len(bcycles)
	b2=0
	b3=0
	b4=None
	b5 = None
	bmore = None
	if(K>3):
		b4=0
		b5 = 0
		bmore = 0

	for c in bcycles:
		if(len(c) == 1):
			print("something went wrong length of blocking cycle 1")
			exit(1)
		if(len(c) == 2):
			b2+=1
		elif(len(c) == 3):
			b3+=1
		elif(len(c) ==4):
			b4+=1
		elif(len(c) == 5):
			b5+=1
		else:
			bmore+=1
	return b,b2,b3,b4,b5,bmore, nvert_improve, aver_nbc_improve_vert

def make_model(mod,m, vert=None):
	strt = process_time()

	model = grb.Model()

	prepare(model)
	model.ModelSense = -1
	coefsunit = {}
	coefsw = {}
	y = None
	lpfile = "lp.lp"
	for a in m.arcs:
		if (a[0] == a[1]):
			coefsunit[a] = 0.0
			coefsw[a] = 0.0
		else:
			coefsunit[a] = 1.0
			if (m.w[a] > 1):
				print("something wrong with weight: >1")
				exit()
			#coefsw[a] = 1 - m.w[a]
			coefsw[a] = round(1 - m.w[a],4)

	# =========== to find maximal nubmer of transplants =================
	if (mod == 111):
		y = m.add_y_vars(model, coefsunit, grb.GRB.BINARY,vert)
		m.add_packing_edge_in(model, y)
		m.add_packing_edge_out(model, y)
		lpfile = "maxt.lp"

	if (mod == 112):
		#coefsx = [len(c) for c in m.C]
		coefsx = [0 for c in m.C]
		y = m.add_y_vars(model, coefsunit, grb.GRB.CONTINUOUS, vert)
		x = m.add_x_vars(model, coefsx, grb.GRB.BINARY)
		m.add_packing(model, x)
		m.add_connection(model, x, y)
		lpfile = "cf.lp"

	# =========== to find maximal weighted transplants ==============

	if (mod == 222):
		y = m.add_y_vars(model, coefsw, grb.GRB.BINARY,vert)
		m.add_packing_edge_in(model, y)
		m.add_packing_edge_out(model, y)
		lpfile = "maxw.lp"

	if (mod == 223):
		#coefsx = [len(c) for c in m.C]
		coefsx = [0 for c in m.C]
		y = m.add_y_vars(model, coefsw, grb.GRB.CONTINUOUS, vert)
		x = m.add_x_vars(model, coefsx, grb.GRB.BINARY)
		m.add_packing(model, x)
		m.add_connection(model, x, y)
		lpfile = "cf.lp"


	# ========== core solution =================
	if (mod == 11):
		y = m.add_y_vars(model, coefsunit, grb.GRB.BINARY,vert)
		p = m.add_p_vars(model, grb.GRB.INTEGER)
		m.add_packing_edge_in(model, y)
		m.add_packing_edge_out(model, y)
		m.add_edge_core(model, y, p)
		lpfile = "coret.lp"

	# ========== core competitive equilibirum =================

	if (mod == 12):
		y = m.add_y_vars(model, coefsunit, grb.GRB.BINARY,vert)
		p = m.add_p_vars(model, grb.GRB.INTEGER)
		m.add_packing_edge_in(model, y)
		m.add_packing_edge_out(model, y)
		m.add_edge_core(model, y, p)
		m.add_edge_competitive(model, y, p)
		lpfile = "competitivet.lp"
	# ========== strong core ==================================
	if (mod == 13):
		y = m.add_y_vars(model, coefsunit, grb.GRB.BINARY,vert)
		p = m.add_p_vars(model, grb.GRB.INTEGER)
		m.add_packing_edge_in(model, y)
		m.add_packing_edge_out(model, y)
		m.add_edge_core(model, y, p)
		m.add_edge_competitive(model, y, p)
		m.add_edge_strong_core(model, y, p)
		lpfile = "core_strong.lp"

	if(mod == 14 or mod ==15 or mod ==16):
		coefsx = [0.0 for c in m.C]
		x = m.add_x_vars(model, coefsx, grb.GRB.BINARY)
		y = m.add_y_vars(model, coefsunit, grb.GRB.CONTINUOUS, vert)
		coefs_tau = [-len(m.V)*len(m.V) for c in m.C]
		tau = m.add_slack_vars(model, coefs_tau, grb.GRB.BINARY)
		m.add_packing(model, x)
		m.add_connection(model, x, y)
		if (mod == 14):
			m.add_edge_stability(model, y, tau)
			lpfile = "cef1y.lp"
		if (mod == 15):
			m.add_edge_Wako_stability(model, y, tau)
			lpfile = "scef1y.lp"
		if (mod == 16):
			m.add_edge_strong_stability(model, y, tau)
			lpfile = "scef1y.lp"

		model.update()



	# ======= weighted core solution =================
	if (mod == 21):
		y = m.add_y_vars(model, coefsw, grb.GRB.BINARY,vert)
		p = m.add_p_vars(model, grb.GRB.INTEGER)
		m.add_packing_edge_in(model, y)
		m.add_packing_edge_out(model, y)
		m.add_edge_core(model, y, p)
		lpfile = "corew.lp"

	# ======= weighted core competitive equilibirum =================
	if (mod == 22):
		y = m.add_y_vars(model, coefsw, grb.GRB.BINARY,vert)
		p = m.add_p_vars(model, grb.GRB.INTEGER)
		m.add_packing_edge_in(model, y)
		m.add_packing_edge_out(model, y)
		m.add_edge_core(model, y, p)
		m.add_edge_competitive(model, y, p)
		lpfile = "competitivew.lp"

	# ===  weighted strong core ==================================
	if (mod == 23):
		y = m.add_y_vars(model, coefsw, grb.GRB.BINARY,vert)
		p = m.add_p_vars(model, grb.GRB.INTEGER)
		m.add_packing_edge_in(model, y)
		m.add_packing_edge_out(model, y)
		m.add_edge_core(model, y, p)
		m.add_edge_competitive(model, y, p)
		m.add_edge_strong_core(model, y, p)
		lpfile = "core_strongw.lp"

	if (mod == 24 or mod == 25 or mod == 26):
		coefsx = [0.0 for c in m.C]
		x = m.add_x_vars(model, coefsx, grb.GRB.BINARY)
		y = m.add_y_vars(model, coefsw, grb.GRB.CONTINUOUS, vert)
		coefs_tau = [-len(m.V) * len(m.V) for c in m.C]
		tau = m.add_slack_vars(model, coefs_tau, grb.GRB.BINARY)
		m.add_packing(model, x)
		m.add_connection(model, x, y)
		if (mod == 24):
			m.add_edge_stability(model, y, tau)
			lpfile = "cef1y.lp"
		if (mod == 25):
			m.add_edge_Wako_stability(model, y, tau)
			lpfile = "scef1y.lp"
		if (mod == 26):
			m.add_edge_strong_stability(model, y, tau)
			lpfile = "scef1y.lp"

		model.update()

	## ====== Quint-Wako model ========================
	# if(mod == 11):

	model.update()
	return model,y, lpfile

def model(mod, resfile, newline, endline, incumbent = None):

	model,y, lpfile = make_model(mod,m)
	#model.write(lpfile)
	if(incumbent!=None):
		for a in arcs:
			y[a].Start = ttc_sol[a]
	optimise_model(model, y, mod, resfile, newline, endline)
	#input()
	#enumerate_solutions(model, y, mod, "enumerate.out")

def enumerate_solutions(model,y,m,file):
	model.setParam("TimeLimit", maxtime)
	if (presolve == 0):
		model.setParam("presolve", 0)
	print("presolve", model.getParamInfo('presolve'))

	return

def calc_trans(sol):
	tr_obj = 0
	for a in sol:
		if a[0] == a[1]:
			continue
		tr_obj += 1
	return tr_obj

def calc_weight(sol,w):
	wobj = 0.0
	for a in sol:
		wobj += 1 - w[a]
	return wobj





def optimise_model(model, y_vars, mod, fileoutres, nl,eol):

	model.setParam("TimeLimit", maxtime)
	if(presolve == 0):
		model.setParam("presolve", 0)
	print("presolve", model.getParamInfo('presolve'))


	gap = None
	gapint = None
	bestbound = None
	mipobj = None
	# =============== mip solve =======================

	strt = process_time()


	# m_pres = model.presolve()
	# m_pres.write(str(mod)+"pres.lp")
	# input()
	model.optimize()
	# status codes:
	#	2 - optimal
	#	9 - time limit
	#	3 - infeasible
	status = model.status
	if (status != 2 and status != 9 and status != 3):
		print("Model status %d. Exiting" % model.status)
		exit(1)
	mipt = process_time() - strt

	print("Mip solved with status", status, "in ", mipt, " seconds")

	price_fairness_max = None
	price_fairness_weight = None
	price_fairness_weight_max = None

	sb = None
	sb_2= None
	sb_3 = None
	sb_4 = None
	sb_5 = None
	sb_more = None

	wb = None
	wb_2 = None
	wb_3 = None
	wb_4 = None
	wb_5 = None
	wb_more = None
	num_trans = None
	tot_weight = None
	s_nv_better =None
	s_av_c_better = None
	w_nv_better = None
	w_av_c_better = None

	if (status == 2 or status == 9):
		mipobj = model.objVal
		sol = {a for a in arcs if y_vars[a].x > 0.5}
		num_trans = calc_trans(sol)
		tot_weight = calc_weight(sol, m.w)
		if(mipobj > 0):

			if (status == 9):
				bestbound = model.ObjBoundC
			if(mod == 111):
				global MaxNumTrans
				MaxNumTrans = mipobj
				global AggTrans
				AggTrans+=mipobj
				global AggTrans_weight
				AggTrans_weight+=tot_weight
			elif(mod == 222):
				global MaxWeightTrans
				MaxWeightTrans = mipobj
				global AggWeight
				AggWeight += mipobj
				global AggWeight_trans
				AggWeight_trans += num_trans
				price_fairness_weight_max = 1.0 * (MaxNumTrans - num_trans) / MaxNumTrans * 100
				global PFweight_trans
				PFweight_trans+=price_fairness_weight_max
				print(price_fairness_weight_max)

			# ------------ price of fairness + global variables saved -------------------------

			elif(mod>10 and mod<20):
				price_fairness_max = 1.0*(MaxNumTrans - mipobj)/MaxNumTrans*100

				if (mod == 11):
					global PFt_core
					PFt_core += price_fairness_max
					global AggTr_core
					AggTr_core += mipobj
				if (mod == 12):
					global PFt_compet
					PFt_compet += price_fairness_max
					global AggTr_compet
					AggTr_compet += mipobj
				if (mod == 13):
					global PFt_stcore
					PFt_stcore += price_fairness_max
					global AggTr_stcore
					AggTr_stcore += mipobj
			elif (mod > 20 and mod < 30):
				price_fairness_weight = 1.0*(MaxWeightTrans - mipobj) / MaxWeightTrans * 100
				price_fairness_weight_max = 1.0 * (MaxNumTrans - num_trans) / MaxNumTrans * 100

				if (mod == 21):
					global PFw_core
					PFw_core += price_fairness_weight
					global PFw_core_trans
					PFw_core_trans += price_fairness_weight_max

					global AggW_core
					AggW_core += mipobj
				if (mod == 22):
					global PFw_compet
					PFw_compet += price_fairness_weight
					global PFw_compet_trans
					PFw_compet_trans+=price_fairness_weight_max
					global AggW_compet
					AggW_compet += mipobj
				if (mod == 23):
					global PFw_stcore
					PFw_stcore += price_fairness_weight
					global PFw_stcore_trans
					PFw_stcore_trans +=price_fairness_weight_max
					global AggW_stcore
					AggW_stcore += mipobj

			#sb_2, sb_3, sb_4, sb_5, sbmore = check_blocking_cycles(sol, 'S', 3)
			#sb_2, sb_3, sb_4, sb_5, sbmore = check_blocking_cycles(sol, 'W', 3)
			print("Check blocking cycles K=", K)
			#sb, sb_2, sb_3, sb_4, sb_5, sb_more = check_blocking_cycles(sol, 'S', len(V))
			#wb, wb_2, wb_3, wb_4, wb_5, wb_more = check_blocking_cycles(sol, 'W', len(V))
			sb, sb_2, sb_3, sb_4, sb_5, sb_more, s_nv_better, s_av_c_better = check_blocking_cycles(m,sol, 'S', K)
			wb, wb_2, wb_3, wb_4, wb_5, wb_more, w_nv_better, w_av_c_better = check_blocking_cycles(m,sol, 'W', K)

			print("Blocking cycles: sb", sb, " wb ", wb)
			## ----- blocking for max n trans --------------------
			if (mod == 111):
				global SBtr5
				SBtr5 += sb
				global WBtr5
				WBtr5 += wb

				global SBtr4
				SBtr4 += sb_2+sb_3+sb_4
				global WBtr4
				WBtr4 += wb_2+wb_3+wb_4

				global SBtr3
				SBtr3 += sb_2 + sb_3
				global WBtr3
				WBtr3 += wb_2 + wb_3

				global SBtr2
				SBtr2 += sb_2
				global WBtr2
				WBtr2 += wb_2

			if (mod == 11):
				global WBt_core5
				WBt_core5 += wb
				global WBt_core4
				WBt_core4 += wb_2+wb_3+wb_4
				global WBt_core3
				WBt_core3 += wb_2+wb_3
				global WBt_core2
				WBt_core2 += wb_2
			if (mod == 12):
				global WBt_compet5
				WBt_compet5 += wb
				global WBt_compet4
				WBt_compet4 += wb_2+wb_3+wb_4
				global WBt_compet3
				WBt_compet3 += wb_2+wb_3
				global WBt_compet2
				WBt_compet2 += wb_2

			## ----- blocking for max weight trans --------------------
			if (mod == 222):
				global SBweight5
				SBweight5 += sb
				global WBweight5
				WBweight5 += wb
				global SBweight4
				SBweight4 += sb_2 + sb_3 + sb_4
				global WBweight4
				WBweight4 += wb_2 + wb_3 + wb_4

				global SBweight3
				SBweight3 += sb_2 + sb_3
				global WBweight3
				WBweight3 += wb_2 + wb_3

				global SBweight2
				SBweight2 += sb_2
				global WBweight2
				WBweight2 += wb_2

			if (mod == 21):
				global WBw_core5
				WBw_core5 += wb
				global WBw_core4
				WBw_core4 += wb_2 + wb_3 + wb_4
				global WBw_core3
				WBw_core3 += wb_2 + wb_3
				global WBw_core2
				WBw_core2 += wb_2
			if (mod == 22):
				global WBw_compet5
				WBw_compet5 += wb
				global WBw_compet4
				WBw_compet4 += wb_2 + wb_3 + wb_4
				global WBw_compet3
				WBw_compet3 += wb_2 + wb_3
				global WBw_compet2
				WBw_compet2 += wb_2
	else:
		if (mod != 13 and mod !=23):
			print("Something went wrong, usolved model is not strong core (13, 23)", mod)
			exit()
		if (mod == 13):
			global Nsolved_Tstcore
			Nsolved_Tstcore -= 1
		if (mod == 23):
			global Nsolved_Wstcore
			Nsolved_Wstcore -= 1

	print("Mip solved =", mipobj, " with status", status, "in ", mipt, " seconds")

	global AggTime
	AggTime+=mipt

	# -------------- write results ------------------------
	ftex = open(fileoutres, 'a')
	if(nl == True):
		ftex.write(instname + "\t" + str(len(V)) + "\t" + str(len(arcs)) + "\t" + str(presolve))# +"\t" + "%.2f" % preptime + "\t"

	ftex.write("\t" + str(mod) + "\t" + str(status)+"\t" + str(mipobj) +"\t"+ str(mipt)+
				"\t" + str(num_trans) + "\t" + str(tot_weight)+
			   "\t"+str(sb)+"\t" + str(sb_2) + "\t" + str(sb_3) + "\t" + str(sb_4) + "\t" + str(sb_5) + "\t" + str(sb_more)+
				"\t" + str(s_nv_better) + "\t" + str(s_av_c_better)+
			   "\t"+str(wb)+"\t" + str(wb_2) + "\t" + str(wb_3) + "\t" + str(wb_4) + "\t" + str(wb_5) + "\t" + str(wb_more)+
				"\t" + str(w_nv_better) + "\t" + str(w_av_c_better))

	if(mod>10 and mod<20):
		ftex.write("\t"+str(price_fairness_max))
	if(mod>20 and mod<30):
		ftex.write("\t" + str(price_fairness_weight))
	if (mod > 20 and mod < 30) or mod == 222:
		ftex.write("\t" + str(price_fairness_weight_max))

	if (el):
		ftex.write("\n")
	ftex.close()
# -----------------------------------------------------

	# if (mod == 888 or mod == 999 or mod == 222):
	# 	global ObjMaxCF
	# 	ObjMaxCF = mipobj
	# 	if(kappa < 0 and mod == 999):
	# 		error("No kappa defined. No relaxing stability calculations" )
	# 	if(mod == 999):
	# 		global rhsdelta
	# 		rhsdelta = ObjMaxCF - round(kappa * ObjMaxCF)
	# 		writerespirce(mod, opt, None, None, inittime, mipt, newline, endline)
	#
	# 	if(mod == 888 or mod ==222):
	# 		writeres(mod, None, mipobj, None, None, opt, inittime,
	# 			 None, mipt, ncols, nrows, nnze, newline, endline)

if __name__ == '__main__':
	import sys

	try:
		preferences = sys.argv[5]

	except:
		preferences = "S"

	print("======================================================")
	print("\t Run settings:")
	print("\t\tpreferences \t", preferences)
	print("======================================================")

	#Sizes = [3]
	#Sizes = [20,30,40,50,60,70,80,90,100,110,120,130,140,150]
	Sizes = [10]

	#numinst = 3
	instancesset = range(3)


	#instancesset = [10]

	# modcodes = [222, 20, 30, 33]

	for size in Sizes:
		# for inst in range(numinst):

		AggTime = 0.0
		Nsolved_Tstcore = len(instancesset)
		Nsolved_Wstcore = len(instancesset)

		AggTrans = 0
		AggTr_core = 0
		AggTr_compet = 0
		AggTr_stcore = 0
		AggWeight_trans = 0
		AggW_core_trans = 0
		AggW_compet_trans = 0
		AggW_stcore_trans = 0

		AggWeight = 0
		AggW_core = 0
		AggW_compet = 0
		AggW_stcore = 0
		AggTrans_weight = 0
		AggTr_core_weight = 0
		AggTr_compet_weight = 0
		AggTr_stcore_weight = 0


		PFt_core = 0.0
		PFt_compet = 0.0
		PFt_stcore = 0.0
		PFweight_trans = 0.0
		PFw_core_trans = 0.0
		PFw_compet_trans = 0.0
		PFw_stcore_trans = 0.0

		PFw_core = 0.0
		PFw_compet = 0.0
		PFw_stcore = 0.0

		SBtr5 = 0
		SBweight5 = 0
		SBtr4 = 0
		SBweight4 = 0
		SBtr3 = 0
		SBweight3 = 0
		SBtr2 = 0
		SBweight2 = 0

		WBtr5 = 0
		WBt_core5 = 0
		WBt_compet5 = 0
		WBtr4 = 0
		WBt_core4 = 0
		WBt_compet4 = 0
		WBtr3 = 0
		WBt_core3 = 0
		WBt_compet3 = 0
		WBtr2 = 0
		WBt_core2 = 0
		WBt_compet2 = 0

		WBweight5 = 0
		WBw_core5 = 0
		WBw_compet5 = 0
		WBweight4 = 0
		WBw_core4 = 0
		WBw_compet4 = 0
		WBweight3 = 0
		WBw_core3 = 0
		WBw_compet3 = 0
		WBweight2 = 0
		WBw_core2 = 0
		WBw_compet2 = 0

		for inst in instancesset:
			if (preferences == "S"):
				filename = "KEP_strict/%d_%d.input" % (size, inst + 1,)
				outfolder = "Strict_results/"
			elif (preferences == "N"):
				filename = "KEP_ties/n%d_%d.input" % (size, inst + 1,)
				outfolder = "Weak_results/"
			else:
				print("Error: Preferences parameter not defined: S or N")
				exit(1)

			sstr1 = filename.replace("KEP_ties/", "")
			sstr = sstr1.replace("KEP_strict/", "")
			instname = sstr.replace(".input", "")
			starttime = process_time()

			V, arcs, w, r = read_data(filename)

			# ======================================
			maxr = max(r[a] for a in arcs)
			if (maxr > len(V)):
				print("something wrong with max rank, should be <=V")
				exit(1)

			# ======================================

			#------- add self loops, if they does not exist
			random.seed(1)

			for i in V:
				if((i,i) not in arcs):
					arcs.add((i, i))
					r[i, i] = len(V) + 1
					w[i, i] = 1.0

			# print(arcs)
			# input()

			adj_in, adj_out = makeadj(arcs, V, r)
			# ----------------------------------------------
			# arcs are already with dummy acrs to NDDs.
			# This code only works for K = L
			# ----------------------------------------------


			if(size<=10):
				K=len(V)
			else:
				K=5
			adj_out_copy = copy.deepcopy(adj_out)
			cycles = TTC(adj_out_copy,r)

			ttc_sol = {a:0.0 for a in arcs}
			for c in cycles:
				for i in range(len(c)):
					ttc_sol[(c[i-1],c[i])] = 1.0
			# cycles = get_all_cycles(V, adj_out, K)
			# numCycles = len(cycles)
			#
			# print("Cycles and chains found:", numCycles)

			#---------------------


			m = modelling()
			m.V = V
			m.arcs = arcs
			m.r = r
			m.adj_out = adj_out
			m.adj_in = adj_in
			m.w = w

			file_full_out = "full.out"
			modcodes = [111,11,12,13]
			for mm in modcodes:
				nl = False
				el = False
				if (modcodes.index(mm) == 0):
					nl = True
				if (modcodes.index(mm) == len(modcodes) - 1):
					el = True

				#model(mm, outfolder +"max_trans.out", nl, el)
				file_full_out = outfolder + "max_trans.out"
				model(mm,  file_full_out, nl, el,ttc_sol)
				print("model %d is done \n================================\n\n" % (mm,))
			#	input()
			if inst == len(instancesset) - 1:
				fff = open(file_full_out, "a")
				fff.write("\n\n\n")
				fff.close()

			modcodes = [222, 21,22,23]
			for mm in modcodes:
				nl = False
				el = False
				if (modcodes.index(mm) == 0):
					nl = True
				if (modcodes.index(mm) == len(modcodes) - 1):
					el = True
				#model(mm, outfolder +"max_weight.out", nl, el)
				file_full_out = outfolder + "max_weight.out"
				model(mm, file_full_out, nl, el,ttc_sol)
				print("model %d is done \n================================\n\n" % (mm,))

			if inst == len(instancesset) - 1:
				fff = open(file_full_out, "a")
				fff.write("\n\n\n")
				fff.close()

		# ===============================================================================
		# ======== print average values ================================================
		# ===============================================================================

		#----- max number of transplants --------------------------

		fpic = open(outfolder + "abs_trans.out", 'a')
		fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%d\n") % (size,1.0 * AggTrans / len(instancesset),
												 1.0 * AggTr_core / len(instancesset),
												 1.0 * AggTr_compet / len(instancesset),
												 1.0 * AggTr_stcore / Nsolved_Tstcore,
														   Nsolved_Tstcore))
		fpic.close()

		# ----- max weight of transplants --------------------------

		fpic = open(outfolder +"abs_weight.out", 'a')
		fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%d\n") % (size, 1.0 * AggWeight / len(instancesset),
												 1.0 * AggW_core / len(instancesset),
												 1.0 * AggW_compet / len(instancesset),
												 1.0 * AggW_stcore / Nsolved_Wstcore,
														   Nsolved_Wstcore))
		fpic.close()

		#----- price of fairness max trans --------------------------
		fpic = open(outfolder + "pf_trans.out", 'a')
		fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n")%
				   (size, 1.0*PFt_core/len(instancesset),
					1.0*PFt_compet/len(instancesset),
					1.0*PFt_stcore/ Nsolved_Tstcore ,
				   	1.0*PFweight_trans/len(instancesset),
				   	1.0*PFw_core_trans/len(instancesset),
				   	1.0*PFw_compet_trans/len(instancesset),
				   	1.0*PFw_stcore_trans/ Nsolved_Wstcore
					)
				   )
		fpic.close()

		# ----- price of fairness max weight --------------------------
		fpic = open(outfolder + "pf_weight.out", 'a')
		fpic.write(("%d\t%.2f\t%.2f\t%.2f\n")%(size, 1.0*PFw_core/len(instancesset),
											   1.0*PFw_compet/len(instancesset),
											   1.0*PFw_stcore/ Nsolved_Wstcore, ))
		fpic.close()

		# ------- blocking cycles max trans -------------------------------------
		fpic = open(outfolder + "strongly_blocking_trans.out", 'a')
		fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\n") % (size, 1.0 * SBtr2 / len(instancesset),
										   1.0 * SBtr3 / len(instancesset),
										   1.0 * SBtr4 / len(instancesset),
										   1.0 * SBtr5 / len(instancesset),
												 ))
		fpic.close()
		# ------- blocking cycles max weight-------------------------------------
		fpic = open(outfolder + "strongly_blocking_weight.out", 'a')
		fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\n") % (size, 1.0 * SBweight2 / len(instancesset),
													   1.0 * SBweight3 / len(instancesset),
													   1.0 * SBweight4 / len(instancesset),
													   1.0 * SBweight5 / len(instancesset),
													   ))
		fpic.close()

		# -------weakly blocking cycles max trans-------------------------------------
		fpic = open(outfolder + "weakly_blocking_trans.out", 'a')
		fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n") % (size,
												 1.0 * WBtr2 / len(instancesset),
												 1.0 * WBtr3 / len(instancesset),
												 1.0 * WBtr4 / len(instancesset),
												 1.0 * WBtr5 / len(instancesset),
												 1.0 * WBt_core2 / len(instancesset),
												 1.0 * WBt_core3 / len(instancesset),
												 1.0 * WBt_core4 / len(instancesset),
												 1.0 * WBt_core5 / len(instancesset),
												 1.0 * WBt_compet2 / len(instancesset),
												 1.0 * WBt_compet3 / len(instancesset),
												 1.0 * WBt_compet4 / len(instancesset),
												 1.0 * WBt_compet5 / len(instancesset),

												 ))
		fpic.close()


		# -------weakly blocking cycles max weight-------------------------------------
		fpic = open(outfolder + "weakly_blocking_weight.out", 'a')
		fpic.write(("%d\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n")% (size,
					  1.0 * WBweight2 / len(instancesset),
					  1.0 * WBweight3 / len(instancesset),
					  1.0 * WBweight4 / len(instancesset),
					  1.0 * WBweight5 / len(instancesset),
					  1.0 * WBw_core2 / len(instancesset),
					  1.0 * WBw_core3 / len(instancesset),
					  1.0 * WBw_core4 / len(instancesset),
					  1.0 * WBw_core5 / len(instancesset),
					  1.0 * WBw_compet2 / len(instancesset),
					  1.0 * WBw_compet3 / len(instancesset),
					  1.0 * WBw_compet4 / len(instancesset),
					  1.0 * WBw_compet5 / len(instancesset),
					  ))
		fpic.close()

# global PFt_core
# PFt_core = 0.0
# global PFt_compet
# PFt_compet = 0.0
# global PFt_stcore
# PFt_stcore = 0.0
#
# global PFw_core
# PFw_core = 0.0
# global PFw_compet
# PFw_compet = 0.0
# global PFw_stcore
# PFw_stcore = 0.0
#
# global SBtr
# SBtr = 0
# global SBweight
# SBweight = 0
#
# global WBtr
# WBtr = 0
# global WBt_core
# WBt_core = 0
# global WBt_compet
# WBt_compet = 0
#
# global WBweight
# WBweight = 0
# global WBw_core
# WBw_core = 0
# global WBw_compet
# WBw_compet = 0
