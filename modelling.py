import gurobipy as grb

class modelling(object):
	def __init__(self):
		self.V = []
		self.arcs = {}
		self.r = {}
		self.w = {}
		self.adj_out = {}
		self.adj_in = {}
		self.C=[]
		self.vinc = None
		self.ainc = None

# ================================================================
# ========== creation of variables ===============================
# ================================================================

	def add_y_vars(self,model,  coefs,  type, vert=None):
		y = {}


		for a in self.arcs:
			if(vert!=None):
				coefs[a] = 0.0
				if(a[0] == vert):
					coefs[a]=len(self.V)+1 - self.r[a]
			y[a] = model.addVar(obj=coefs[a], vtype=type, name="y%d,%d" % (a[0], a[1],), lb=0.0, ub=1.0)
		model.update()
		return y



	def add_p_vars(self, model, type):
		p = []
		for v in self.V:
			p.append(model.addVar(obj = 0.0, vtype = type, name = "p%d" %(v), lb=1.0, ub= len(self.V)))
		model.update()
		return p


##--- varibales for bounded case ----------------------------

	def add_x_vars(self,model, coefs,  type):
		x = []
		i = 0
		for c in self.C:
			x.append(model.addVar(obj=coefs[i], vtype=type, name="x%d" % (i,),
							 lb=0.0, ub=1.0))
			i += 1
		model.update()
		return x

	# def add_y_vars(self,model,  coefs,  type):
	# 	y = {}
	# 	for a in self.arcs_dummy:
	# 		# y[a] = model.addVar(obj=1.0,  name="y%d,%d" % (a[0], a[1],), lb=0.0, ub=1.0)
	# 		y[a] = model.addVar(obj=coefs[a], vtype=type, name="y%d,%d" % (a[0], a[1],), lb=0.0, ub=1.0)
	# 		if(len(self.ainc[a]) == 0):
	# 			y[a].ub = 0.0
	# 	model.update()
	# 	return y

	def add_slack_vars(self,model, coefs,type):
		tau = []
		i = 0
		for c in self.C:
			tau.append(model.addVar(obj=coefs[i], vtype=type, name="tau%d" % (i,), lb=0.0))
			i += 1
		model.update()
		return tau

## ===========================================================================================================
## =========== constraints for unbounded edge formulation ====================
## ===========================================================================================================

	def add_edge_core(self, model,y,p, slack=None):

		#model.addConstrs(len(self.V) * grb.quicksum(y[k,a[1]]	for k in self.adj_in[a[1]]
		#				if self.r[k,a[1]] <= self.r[a[0],a[1]]) +p[a[0]] - p[a[1]] >=1	 for a in self.arcs)
		if(slack == None):
			model.addConstrs((len(self.V) * grb.quicksum(y[i, k] for k in self.adj_out[i]
						if self.r[i,k] <= self.r[i,j] ) +p[j] - p[i] >=1	 for (i,j) in self.arcs ))#), name = "core_")
			# model.addConstrs((len(self.V) * grb.quicksum(y[i, k] for k in self.adj_out[i]
			# 			if self.r[i,k] <= self.r[i,j] ) +p[j] - p[i] >=1	 for (i,j) in self.arcs if i!=j), name = "core_")

		else:
			model.addConstrs(len(self.V) * grb.quicksum(y[i, k] for k in self.adj_out[i]
														if self.r[i, k] <= self.r[i, j]) + p[j] - p[i]  + slack[i,j]>= 1 for (i, j)
							 							in self.arcs)

	def add_edge_competitive(self, model,y,p, slack = None):
		if(slack == None):
			model.addConstrs((len(self.V) * (1 - y[i, j]) + p[j] - p[i] >= 0 for (i, j) in self.arcs ))#, name = "compet_")
			#model.addConstrs((len(self.V) * (1 - y[i, j]) + p[j] - p[i] >= 0 for (i, j) in self.arcs if i!=j), name="compet_")

		else:
			model.addConstrs(len(self.V) * (1 - y[i, j]) + p[j] - p[i] + slack[i,j]>= 0 for (i, j) in self.arcs)

	def add_edge_strong_core(self, model,y,p, slack = None):

		#model.addConstrs(len(self.V) *( y[a[0],a[1]] + grb.quicksum( y[k,a[1]]	for k in self.adj_in_dummy[a[1]]
		#				if self.r[k,a[1]] < self.r[a[0],a[1]])) +p[a[0]] - p[a[1]] >=1	 for a in self.arcs_dummy)

		#!!! remove y_ij
		if(slack == None):
			# model.addConstrs((len(self.V) * (y[i,j] + grb.quicksum(y[i, k] for k in self.adj_out[i]
			# 		if self.r[i,k] < self.r[i,j])) +p[j] - p[i] >=0
			# 				  for (i,j) in self.arcs))#, name= "strcore_")
			model.addConstrs((len(self.V) * (grb.quicksum(y[i, k] for k in self.adj_out[i]
					if self.r[i, k] < self.r[i, j])) + p[j] - p[i] >= 0
							  for (i, j) in self.arcs))  # , name= "strcore_")

		else:
			# model.addConstrs(len(self.V) * (y[i, j] + grb.quicksum(y[i, k] for k in self.adj_out[i]
			# 													   if self.r[i, k] < self.r[i, j])) +
			# 				p[j] - p[i] + slack[i,j]>= 0 for (i, j) in self.arcs)
			model.addConstrs(len(self.V) * (grb.quicksum(y[i, k] for k in self.adj_out[i]
										if self.r[i, k] < self.r[i, j])) +
							p[j] - p[i] + slack[i,j]>= 0 for (i, j) in self.arcs)


# ===========================================================================================================
# ================== constraints allocation =============================================================
# ===========================================================================================================

	def add_packing_edge_in(self, model, y):
		#print("adj_in modelling", self.adj_in)
		for i in self.V:
			coef = []
			vari = []
			for j in self.adj_in[i]:
				coef.append(1)
				vari.append(y[j,i])
			if len(coef) > 0:
				expr = grb.LinExpr(coef, vari)
				model.addConstr(lhs=expr, sense=grb.GRB.EQUAL, rhs=1.0, name="pack_in_%d" % (i,))

	def add_packing_edge_out(self, model, y):
		for i in self.V:
			coef = []
			vari = []
			for j in self.adj_out[i]:
				coef.append(1)
				vari.append(y[i, j])
			if len(coef) > 0:
				expr = grb.LinExpr(coef, vari)
				model.addConstr(lhs=expr, sense=grb.GRB.EQUAL, rhs=1.0, name="pack_out_%d" % (i,))

# ===========================================================================================================
# ================ constraints for bounded allocation =================================================
# ===========================================================================================================

	def add_packing(self,model, x):

		for v in self.V:
			coef = []
			vari = []
			for ic in self.vinc[v]:
				coef.append(1)
				vari.append(x[ic])

			if len(coef) > 0:
				expr = grb.LinExpr(coef, vari)
				#model.addConstr(lhs=expr, sense=grb.GRB.LESS_EQUAL, rhs=1.0, name = "pack_%d"%(v,))
				model.addConstr(lhs=expr, sense=grb.GRB.EQUAL, rhs=1.0, name="pack_%d" % (v,))

	def add_flowconservation(self,model, y):
		for i in self.V:
			coef = []
			vari = []
			for  j in self.adj_in[i]:
				coef.append(1)
				vari.append(y[j,i])
			for j in self.adj_out[i]:
				coef.append(-1)
				vari.append(y[i,j])
			if len(coef) > 0:
				expr = grb.LinExpr(coef, vari)
				model.addConstr(lhs=expr, sense=grb.GRB.EQUAL, rhs=0.0, name="flow_%d" % (i,))


	def add_connection(self,model, x,y):

		#sum (ij)in c x_c= y_ij
		model.addConstrs(grb.quicksum(x[ic] for ic in self.ainc[a])== y[a] for a in self.arcs) #, name ="xa_y")

# ===========================================================================================================
# =================== constraints for bounded stability ==================================================
# ===========================================================================================================


	def add_edge_stability(self, model, y, tau=None):
		b_arcs = []
		for c in self.C:
			ic = self.C.index(c)
			b_arcs.append(set())
			for i in range(len(c)):
				b_arcs[ic].add((c[i - 1], c[i]))
				for v in self.adj_out[c[i-1]]:
					if (self.ainc[c[i-1],v] == []):
						continue
					if (self.r[c[i - 1],v] <= self.r[c[i - 1], c[i]]):
						b_arcs[ic].add((c[i-1],v))
		if (tau == None):
			model.addConstrs(grb.quicksum(y[a] for a in b_arcs[ic]) >= 1.0
							 for ic in range(len(self.C)))  # , name = "stabE")
		else:
			model.addConstrs(grb.quicksum(y[a] for a in b_arcs[ic]) + tau[ic] >= 1.0
							 for ic in range(len(self.C)))  # , name = "stabE")



	def add_edge_strong_stability(self, model, y, tau=None):

		####
		### !!!! this code need to be adjusted for the case when self-loop may have ties
		###	!!! in that case coef for eq_arcs is = len(c) as self loop hav len = 2, should be 1
		###
		b_arcs = []
		eq_arcs = []
		for c in self.C:
			ic = self.C.index(c)
			coef = []
			vari = []
			b_arcs.append(set())
			eq_arcs.append(set())
			for i in range(len(c)):
				if c[i-1] == c[i]:
					b_arcs[ic].add((c[i - 1], c[i]))
				else:
					eq_arcs[ic].add((c[i - 1], c[i]))
				for v in self.adj_out[c[i-1]]:
					if (self.ainc[c[i-1],v] == []):
						continue
					if (self.r[c[i-1],v ] < self.r[c[i - 1], c[i]]):
						b_arcs[ic].add((c[i-1],v))
					if (self.r[c[i-1],v] == self.r[c[i - 1], c[i]] and v != c[i]):
						eq_arcs[ic].add((c[i-1],v))
		if (tau == None):
			model.addConstrs(grb.quicksum(len(self.C[ic]) * y[a] for a in b_arcs[ic]) +
							 grb.quicksum(y[a] for a in eq_arcs[ic]) >= len(self.C[ic])
							 for ic in range(len(self.C)))  # , name = "sstabE")
		else:
			model.addConstrs(grb.quicksum(len(self.C[ic]) * y[a] for a in b_arcs[ic]) +
							 grb.quicksum(y[a] for a in eq_arcs[ic]) + len(self.C[ic]) * tau[ic] >= len(self.C[ic])
							 for ic in range(len(self.C)))  # , name = "sstabE")

	def add_edge_Wako_stability(self,model,y,tau):
		b_arcs = []
		c_arcs = []
		for c in self.C:
			ic = self.C.index(c)
			coef = []
			vari = []
			b_arcs.append(set())
			c_arcs.append(set())
			for i in range(len(c)):
				if(c[i-1] == c[i]):
					b_arcs[ic].add((c[i-1], c[i]))
				else:
					c_arcs[ic].add((c[i - 1], c[i]))
				for v in self.adj_out[c[i - 1]]:
					if (self.ainc[c[i - 1], v] == []):
						continue
					if (self.r[c[i - 1], v] <= self.r[c[i - 1], c[i]] and v != c[i]):
						b_arcs[ic].add((c[i - 1], v))
		if (tau == None):
			model.addConstrs(grb.quicksum(len(self.C[ic]) * y[a] for a in b_arcs[ic]) +
							 grb.quicksum(y[a] for a in c_arcs[ic]) >= len(self.C[ic])
							 for ic in range(len(self.C)))  # , name = "sstabE")
		else:
			model.addConstrs(grb.quicksum(len(self.C[ic]) * y[a] for a in b_arcs[ic]) +
							 grb.quicksum(y[a] for a in c_arcs[ic]) + len(self.C[ic]) * tau[ic] >= len(self.C[ic])
							 for ic in range(len(self.C)))  # , name = "sstabE")

		return

# ===========================================================================================================
# =========== constraints for relaxing stability ============================================================
# ===========================================================================================================

	def add_relax_bound(self, model, coefs, vars, rhs):
		model.addConstr(grb.quicksum(coefs[i] * vars[i] for i in range(len(vars)))
						>= rhs, name="RELAXEDMAX")

	def add_upper_bound_arcs(self, model, coefs, vars, rhs):
		model.addConstr(grb.quicksum(coefs[a] * vars[a] for a in self.arcs)
						>= rhs, name="upperMAX")
