# StableHouseMarket

There are 3 main modules in the project

- run_models.py - implements models for unbounded case (house market)
- run_bounded_models.py - models for the case when the length of exchange cycles is bounded (or stable KEP problem)
- respecting_improvement.py - the module contains implementation of algorithm for checking of the respecting improvement property.


Other modules contain some axillary functionalities.

- enum_cycles.py - contain functions for enumeration of cycles and chains.
- hm_utils.py - contain some auxiliary functions for data structures reorganisation.
- modelling.py - contains implementation functions that add variables and all possible constraints to the models (for both bounded and unbounded cases)
- skep_io.py - contain function for reading the files.


Folders KEP_strict and KEP_ties contain examples of input files with strict preferences and ties respectively. The full dataset is available at (https://doi.org/10.25747/xh4y-2r05)

It is necessary to create empty folders Strict_results and Weak_results for placing output files.

Gurobi Optimizer (https://www.gurobi.com/products/gurobi-optimizer/) is required for running the code.