import gurobipy as grb

def error(s):
	print("!!!!!!!!!!Error.\n\t", s)
	exit(1)


# def comparator(a1, a2):
# 	if r[a1]>r[a2]:
# 		return -1
# 	elif r[a1] == r[a2]:
# 		return 0
# 	else:
# 		return 1
def myf(a,r):
	return r[a]

def makeadj(arcs,V,r):

	adj_out = {}
	adj_in = {}
	arcs_list = []


	for v in V:
		adj_out[v] = []
		adj_in[v] = []
	for a in arcs:
		i = a[0]
		j = a[1]
		adj_out[i].append(j)
		adj_in[j].append(i)

	for i in adj_out.keys():
		adj_out[i] = sorted(adj_out[i], key=lambda x: r[i, x])

	return adj_in, adj_out

