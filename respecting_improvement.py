from skep_io import *
from hm_utils import *
from enum_cycles import *
from time import process_time
from modelling import *
from run_models import make_model
import random
import copy

numthreads = 1

not_respected = 0
count_check_RIP = 0
def prepare(model):
	model.setParam("LogToConsole", 0)
	model.setParam("Threads", numthreads)
	model.setParam("MarkowitzTol ", 1e-4)
	# model.setParam("MIPGap", mymipgap)
	# model.setParam("FeasibilityTol", 1e-9)
	# model.setParam("IntFeasTol", 1e-9)
	# model.setParam("MIPGapAbs", mymipgap)
	# model.setParam("OptimalityTol", 1e-9)
	#model.setParam("Presolve", 0)




	model.update()
	model.optimize()



def make_improvement(type, i,j,r,adj_in, adj_out, arcs, w):

	# types:
	#	1: swap with the previously prefered k Pj i => i Pj k
	#	2: the best i Pj k forall k Rj j

	best_good = False
	arc_exists = False
	new_r = r.copy()
	new_w = w.copy()

	# ********* arc DOES NOT EXIST in the graph ****************
	if(i not in adj_out[j]):
		#print("arc does not exist")
		new_adj_in = copy.deepcopy(adj_in)
		new_adj_out = copy.deepcopy(adj_out)
		new_arcs = copy.deepcopy(arcs)

		if(type == 1):
			#------ change ranking ---------
			# new_r[j,i] = r[j, adj_out[j][-1]]
			# new_r[j,adj_out[j][-1]] = r[j, adj_out[j][-1]]+1
			if (r[j, adj_out[j][-1]]!=len(V) + 1  or adj_out[j][-1]!=j):
				print("kuku!! r", r[j, adj_out[j][-1]], "j", j, adj_out[j][-1])
				input()
			w2=w[j,adj_out[j][-1]]
			if(len(adj_out[j]) == 1):
				w1 = 0.0
			else:
				w1=w[j,adj_out[j][-2]]
			new_w[j, i] = round(w1+(w2-w1)*random.random(), 2)
			new_r[j,i] = len(V)
			# ----- update graphs --------
			new_adj_out[j].append(i)
			new_adj_out[j] = sorted(new_adj_out[j], key=lambda x: new_r[j, x])
			new_adj_in[i].append(j)
			new_arcs.add((j,i))
		elif(type == 2):
			# ------ change ranking ---------
			new_r[j,i] = 0

			# ----- update graphs --------
			new_adj_out[j].insert(0, i)
			new_adj_in[i].append(j)
			new_arcs.add((j, i))
		return best_good, arc_exists, new_arcs, new_adj_in, new_adj_out, new_r, new_w


	# ********  arc EXISTS in the graph ****************
	else:
		#print("arc exists")
		arc_exists = True
		# i is the best for j already
		if (r[j, i] == r[j, adj_out[j][0]]):
			#print("is the BEST arc j,i", j, i)
			best_good = True
			return best_good, arc_exists, arcs, adj_in, adj_out, r, w
		# i is not the best for j
		else:
			new_adj_out = copy.deepcopy(adj_out)
			if(type == 1):
				if(preferences=='S'):
					ind_i = adj_out[j].index(i)
					prev = adj_out[j][ind_i-1]
					if (r[j, prev] == r[j, i]):
						print("Attention!!! It seems something went wrong j=", j, "kk=", prev, "i = ", i,
							  r[j, prev], "=", r[j, i])
						#print(r)
						input()

					new_r[j,prev]= r[j,i]
					new_r[j,i] = r[j,prev]
					if(w!=None):
						new_w[j, i] = w[j, prev]
						new_w[j, prev] = w[j, i]

				elif (preferences == 'N'):
					ind_i = adj_out[j].index(i)
					for k in range(ind_i):
						kk = adj_out[j][ind_i - k - 1]
						if (r[j, kk] < r[j, i]):
							prev = kk
							break
					new_r[j, i] = r[j, prev]
					if (w != None):
						new_w[j, i] = w[j,prev]+0.001

					# for k in range(ind_i):
					# 	kk = adj_out[j][ind_i - k - 1]

					# if (r[j, kk] < r[j, i]):
					# 	# ------ change ranking ---------
					# 	new_r[j, i] = r[j, kk]
					# 	new_r[j, kk] = r[j, i]
					# 	if(w!=None):
					# 		new_w[j, i] = w[j, kk]
					# 		new_w[j, kk] = w[j, i]
					#

					# ----- reorder graph --------

				new_adj_out[j] = sorted(new_adj_out[j], key=lambda x: new_r[j, x])

			elif(type == 2):
					# ----- change ranking --------
				new_r[j,i] = 0

				# ----- reorder graph --------
				new_adj_out[j] = sorted(new_adj_out[j], key=lambda x: new_r[j, x])

			return best_good, arc_exists, arcs, adj_in, new_adj_out, new_r, new_w

def make_new_cycles(adj_out,a, C, ainc, vinc):

	new_cycles = get_all_cycles(V, adj_out, K, checker = lambda c: c not in C)

	new_ainc = copy.deepcopy(ainc)
	new_ainc[a] = []
	if(len(new_cycles) > 0):
		new_C = copy.deepcopy(C)
		new_vinc = copy.deepcopy(vinc)
		for c in new_cycles:
			new_C.append(c)
			ic = new_C.index(c)
			for iv in range(len(c)):
				a = (c[iv - 1], c[iv])
				new_ainc[a].append(ic)
				new_vinc[c[iv]].append(ic)
		return new_C,new_vinc, new_ainc
	else:
		return C,vinc, new_ainc


def get_arc_in_sol(i,sol):
	for a in sol:
		if a[0] == i:
			return a
	return None

def find_best_allotment(mod,m,i, obj_max = None):

	feasibility = True
	if(mod==111 or mod == 222 or mod == 112 or mod == 223):
		model, y, lp = make_model(mod, m)
		# print(obj_max)
		if(obj_max == None):
			model.optimize()
			#model.write("lpbest1.lp")
			obj_max = model.objVal
		for a in m.arcs:
			y[a].Obj = 0.0
			if(a[0] == i):
				y[a].Obj=len(V)+2-m.r[a]
		coefs = {}
		for a in m.arcs:
			if a[0] == a[1]:
				coefs[a] = 0.0
			else:
				if(mod >200):
					#coefs[a] = 1.0 - m.w[a]
					coefs[a] = round(1.0 - m.w[a],4)
				else:
					coefs[a] = 1.0

		# print(obj_max)
		m.add_upper_bound_arcs(model, coefs, y, obj_max-1e-5)
		model.update()
		#model.write("lpbest.lp")
		model.optimize()
		# print(model.status)

	else:
		model, y, lp = make_model(mod, m, i)
		#model.write("lp_impr.lp")
		model.optimize()
		if(model.objVal < 0):
			feasibility = False

#	print("lp printed")
#	input()

	# print("obj", model.objVal)
	# for v in model.getVars():
	# 	if(v.x>0.5):
	# 		print(v.varName, end = " ")
	# print()
	# for a in m.arcs:
	# 	if(y[a].x>0.5):
	# 		print(a, end =" ")
	#
	# print()
	return feasibility, {a for a in m.arcs if y[a].x > 0.5}



def RIP(m,mod):

	first_max = None
	if(mod == 111 or mod == 222 or mod == 112 or mod == 223):
		model, y, lp = make_model(mod, m)
		model.optimize()
		# model.write("first.lp")
		first_max = model.objVal
		print("first obj", first_max)
	for i in V:
		m.arcs = arcs
		m.r = r
		m.adj_out = adj_out
		m.adj_in = adj_in
		m.w = w
		m.C = C
		m.vinc = vinc
		m.ainc = ainc
		feasible, i_sol = find_best_allotment(mod,m,i, first_max)
		#print(i, feasible)
		if(not feasible):
			continue
		for j in V:
			if(i==j):
				continue
			m.arcs = arcs
			m.r = r
			m.adj_out = adj_out
			m.adj_in = adj_in
			m.w = w
			m.C = C
			m.vinc = vinc
			m.ainc = ainc
			cur_sol = i_sol
			# print("j,i before bubble start ", j, i)
			# print(m.adj_out)
			# print(m.r)

			while(True):

				best_good, arc_exists, ij_arcs, ij_adj_in, ij_adj_out, ij_r, ij_w = \
					make_improvement(1,i,j, m.r, m.adj_in, m.adj_out, m.arcs, m.w)
				if(best_good):
					break
				# print()
				# print("buble: j,i", j, i, arc_exists)
				# print(ij_adj_out)
				# print(ij_w)
				# print(ij_r)
				# input()


				# ======= update graph and preferences ============

				m.arcs = ij_arcs
				m.r = ij_r
				m.adj_out = ij_adj_out
				m.adj_in = ij_adj_in
				m.w = ij_w
				if(not arc_exists):
					if(mod == 112 or mod>13 and mod<20 or mod == 223 or mod>23 and mod<30):
						m.C, m.vinc, m.ainc = make_new_cycles(m.adj_out, (j,i), m.C, m.ainc, m.vinc)
						#print("newcycles", m.C)


				feasible, new_sol = find_best_allotment(mod, m, i)

				if(not feasible):
					# print("infeasible")
					# print("check cursol", cur_sol)
					# print("new_sol", new_sol)
					# input()
					continue
				#


				a_cur = get_arc_in_sol(i,cur_sol)
				a_new = get_arc_in_sol(i,new_sol)
				#print(a_cur,":", r[a_cur], a_new,":", r[a_new])
				if(a_cur==None or a_new==None):
					print("No arc in solution: ", a_cur, a_new)
					exit()

				if r[a_cur]<r[a_new]:
					# print()
					# print("violation: j,i", j, i, arc_exists)
					# print('adjout', ij_adj_out)
					# print("check cursol", cur_sol)
					# print("new_sol", new_sol)

					# for c in m.C:
					# 	if(c[0]!=c[1]):
					# 		print(c,":", ij_r[c[0],c[1]], ij_r[c[1],c[0]])
					a_cur = get_arc_in_sol(i, cur_sol)
					a_new = get_arc_in_sol(i, new_sol)
					print("Violated", "j=", j, "i=", i,":", a_cur,  r[a_cur],a_new, r[a_new], arc_exists)
					#input()
					global not_respected
					not_respected+=1
				global count_check_RIP
				count_check_RIP+=1
				cur_sol = new_sol
				if(mod == 111 or mod == 112 ):
					break
	print("not RIP", not_respected, count_check_RIP)
	#input()
	return not_respected, count_check_RIP

if __name__ == '__main__':
	import sys

	try:
		preferences = sys.argv[5]
	except:
		preferences = "S"

	print("======================================================")
	print("\t Run settings:")
	print("\t\tpreferences \t", preferences)
	print("======================================================")

	#Sizes = [20,30,40]
	Sizes = [10]
	#numinst = 3
	instancesset = range(3)

	filefolder = ""
	if (preferences == "S"):
		filefolder = "KEP_strict/"
		outfolder = "Strict_results/"
	elif (preferences == "N"):
		filefolder = "KEP_ties/n"
		outfolder = "Weak_results/"
	else:
		print("Error: Preferences parameter not defined: S or N")
		exit(1)
	for size in Sizes:
		# for inst in range(numinst):

		av_nRIP = 0
		av_count_check_RIP = 0
		for inst in instancesset:

			filename = filefolder+"%d_%d.input" % (size, inst+1,)
			sstr1 = filename.replace("KEP_ties/", "")
			sstr = sstr1.replace("KEP_strict/", "")
			instname = sstr.replace(".input", "")
			starttime = process_time()

			V, arcs, w, r = read_data(filename)

			# ======================================
			maxr = max(r[a] for a in arcs)
			if (maxr == len(V)):
				print("something wrong with max rank, should be <=V")
				exit(1)

			# ======================================

			#------- add self loops, if they does not exist
			random.seed(1)
			for i in V:
				if((i,i) not in arcs):
					arcs.add((i, i))
					r[i, i] = len(V) + 1
					w[i, i] = 1.0

			# input()

			adj_in, adj_out = makeadj(arcs, V, r)
			# print(arcs)
			# print(adj_out)
			# print(w)
			m = modelling()


			C = None
			vinc = None
			ainc = None

			# modcodes =[111,11,222,21]
			# #modcodes = [222, 21]
			# outfile1 = outfolder+preferences+"_inf.out"
			# outfile2 = outfolder+preferences+"_check_inf.out"

			modcodes = [112, 14, 15, 16, 223, 24, 25, 26]
			#modcodes = [14,15,16]
			K = 2
			outfile1 = outfolder+preferences +"_"+str(K)+".out"
			outfile2 = outfolder+preferences +"_check" +str(K)+  ".out"

			f1 = open(outfile1, "a")
			f2 = open(outfile2, "a")
			f1.write(instname)
			f2.write(instname)
			f1.close()
			f2.close()

			for mod in modcodes:
				random.seed(mod)
				m.V = V
				m.arcs = arcs
				m.r = r
				m.adj_out = adj_out
				m.adj_in = adj_in
				m.w = w
				m.C = None
				m.vinc = None
				m.ainc = None

				not_respected = 0
				count_check_RIP = 0

				if(mod>13 and mod<20 or mod == 112 or mod == 223 or mod>23 and mod<30):
					C = get_all_cycles(V, adj_out, K)
					numCycles = len(C)

					print("Cycles and chains found")
					for v in V:
						C.append((v,v))
					#print(C)
					# for ic in range(len(C)):
					# 	print(ic, C[ic])
					vinc = {}
					for v in V:
						vinc[v] = [ic for ic in range(len(C)) if v in C[ic]]

					print("vinc made")
					#print(vinc)
					ainc = {}
					for a in arcs:
						ainc[a] = []

					for c in C:
						ic = C.index(c)
						for i in range(len(c)):
							a = (c[i - 1], c[i])
							ainc[a].append(ic)
							if(c[i-1] == c[i]):
								break
					#print(ainc)
					m.C = C
					m.vinc = vinc
					m.ainc = ainc

				nRIP, countRIP = RIP(m,mod)

				f1 = open(outfile1,"a")
				f1.write("\t"+str(nRIP)+"\t")
				f1.close()

				f2 = open(outfile2,"a")
				f2.write("\t"+str(countRIP)+"\t")
				f2.close()

				av_nRIP+=nRIP
				av_count_check_RIP = count_check_RIP
			f1 = open(outfile1, "a")
			f1.write("\n")
			f1.close()

			f2 = open(outfile2, "a")
			f2.write("\n")
			f2.close()